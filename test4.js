const events = require("events");
const testEmit = new events();

function createEmitter(onOpen, onClose) {
testEmit.on('cobaEmitter', onOpen)
testEmit.on('cobaEmitter2',onClose)
return testEmit
  }


function opened(emitter) {
emitter.emit('cobaEmitter')
}

function closed(emitter) {
emitter.emit('cobaEmitter2')
}

let emitter = createEmitter(
  () => console.log("Opened!"),
  () => console.log("Closed!")
);

opened(emitter);
closed(emitter);

module.exports.createEmitter = createEmitter;
module.exports.opened = opened;
module.exports.closed = closed;
