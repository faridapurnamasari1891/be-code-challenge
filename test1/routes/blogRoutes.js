const express = require('express') // Import express
const router = express.Router() // Make router from app
const BlogController = require('../controllers/blogController.js')
const blogValidator = require('../middlewares/validators/blogValidator.js')

router.get('/',BlogController.getAll)
router.get('/:id',blogValidator.getOne,BlogController.getOne)

router.post('/create', blogValidator.create, BlogController.create)
router.put('/update/:id', blogValidator.update, BlogController.update)
router.delete('/delete/:id',blogValidator.delete,  BlogController.delete) // If accessing localhost:3000/transaksi/delete/:id, it will call delete function in TransaksiController class

module.exports = router; // Export router
