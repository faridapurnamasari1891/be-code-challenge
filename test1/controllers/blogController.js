const {
blog
} = require('../models')

class BlogController {

  async getAll(req, res) {

    blog.find({})
    .then(result => {
      res.json({
        status: "Success",
        data: result
        })
    })
  }


  async getOne(req, res) {
    blog.findOne({
      _id: req.params.id
    }).then(result => {
      res.json({
        status: "Success",
        data: result
      })
    })
  }

  async create(req, res) {
    blog.create({
      blogName:req.body.blogName,
      blogDescription:req.body.blogDescription,

    }).then(result => {
      res.json({
        status: "Success",
        data: result
      })
    })
  }

  async update(req, res) {

    //    const total = rate => eval(rate.reduce((a,b) => a+b,0).toString()) / rate.length



    blog.findOneAndUpdate({
      _id: req.params.id
    }, {
      blogName:req.body.blogName,
      blogDescription:req.body.blogDescription,
    }).then(() => {
      return blog.findOne({
        _id: req.params.id
      })
    }).then(result => {
      res.json({
        status: "Success",
        data: result
      })
    })
  }

  async delete(req, res) {
    blog.delete({
      _id: req.params.id
    }).then(() => {
      res.json({
        status: "Success",
        data: null
      })
    })
  }

}

module.exports = new BlogController
