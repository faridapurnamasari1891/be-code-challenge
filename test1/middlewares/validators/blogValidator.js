const {
  blog
} = require('../../models')
const {
  check,
  validationResult,
  matchedData,
  sanitize
} = require('express-validator'); //form validation & sanitize form params


module.exports = {
  getOne: [
    check('id').custom(value => {
      return blog.findOne({
        _id: value
      }).then(result => {
        if (!result) {
          throw new Error('blog does not exist!')
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    },
  ],

  create: [

    check('blogName','Blog name should not empty').exists(),
    
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    },
  ],


  update: [

    check('blogName','Blog name should not empty').exists(),
    check('id').custom(value => {
      return blog.findOne({
        _id: value
      }).then(b => {
        if (!b) {
          throw new Error('blog does not exist');
        }

      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    },
  ],
  delete: [
    check('id').custom(value => {
      return blog.findOne({
        _id: value
      }).then(result => {
        if (!result) {
          throw new Error('blog does not exist!')
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    },
  ]
};
