const mongoose = require('mongoose');


const BlogSchema = new mongoose.Schema({
  blogName: {
    type: String,
    required: true
  },

  blogDescription: {
    type: String,
    default: null,
    required: false
  }
}, {
  timestamps: {
    createdAt: 'blogDate',
    updatedAt: 'updated_at'
  },
  versionKey: false
})



BlogSchema.set('toJSON', {
  getters: true
})



module.exports = blog = mongoose.model('blog', BlogSchema, 'blog');
